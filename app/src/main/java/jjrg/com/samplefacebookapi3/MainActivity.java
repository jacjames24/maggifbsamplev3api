package jjrg.com.samplefacebookapi3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private LoginButton loginBtn;
    private Button postImageBtn;
    private Button updateStatusBtn;

    private TextView userName;

    private UiLifecycleHelper uiHelper;

    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");

    private static String message = "Sample status posted from android app";
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        userName = (TextView) findViewById(R.id.user_name);
        loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
        loginBtn.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    userName.setText("Hello, " + user.getName());
                } else {
                    userName.setText("You are not logged");
                }
            }
        });

        postImageBtn = (Button) findViewById(R.id.post_image);
        postImageBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //postImage();

                    try {
                        // clearing app data
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec("pm clear com.facebook.katana");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

            }
        });

        updateStatusBtn = (Button) findViewById(R.id.update_status);
        updateStatusBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "trying to post a status message");

                //postStatusMessage();
                //share();
                publishFeedDialog();
                //publishPicture(); //- crashes without facebook app
            }
        });

        buttonsEnabled(false);

        //code to null the session for facebook and log it out
//        Session session = Session.getActiveSession();
//
//        if (session!=null){
//            Log.d(TAG, "session is not null");
//
//            if (session.isClosed()){
//                Log.d(TAG, "session is closed");
//            }else{
//                Log.d(TAG, "session is open");
//                Session.getActiveSession().closeAndClearTokenInformation();
//                Log.d(TAG, "clearing the session");
//                Session.setActiveSession(null);
//            }
//        }else{
//            Log.d(TAG, "session is null");
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            if (state.isOpened()) {
                buttonsEnabled(true);
                Log.d(TAG, "Facebook session opened");
            } else if (state.isClosed()) {
                buttonsEnabled(false);
                Log.d(TAG, "Facebook session closed");
//                Session.getActiveSession().closeAndClearTokenInformation();
//                Log.d(TAG, "clearing the session");
//                Session.setActiveSession(null);
            }
        }
    };

    public void buttonsEnabled(boolean isEnabled) {
        postImageBtn.setEnabled(isEnabled);
        updateStatusBtn.setEnabled(isEnabled);
    }

    public void postImage() {
        if (checkPermissions()) {
            Bitmap img = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_launcher);
            Request uploadRequest = Request.newUploadPhotoRequest(
                    Session.getActiveSession(), img, new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {
                            Toast.makeText(MainActivity.this,
                                    "Photo uploaded successfully",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
            uploadRequest.executeAsync();
        } else {
            requestPermissions();
        }
    }

    public void postStatusMessage() {
        if (checkPermissions()) {
            Log.d(TAG, "requested permissions");
            Request request = Request.newStatusUpdateRequest(
                    Session.getActiveSession(), message,
                    new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {
                            if (response.getError() == null)
                                Toast.makeText(MainActivity.this,
                                        "Status updated successfully",
                                        Toast.LENGTH_LONG).show();
                        }
                    });
            request.executeAsync();
        } else {
            Log.d(TAG, "Asking for permission");
            requestPermissions();
        }
    }

    //mine
    public void share(){
        if (FacebookDialog.canPresentShareDialog(getApplicationContext(),
                FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
            // Publish the post using the Share Dialog
            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
                    .setLink("https://developers.facebook.com/android")
                    .build();
            uiHelper.trackPendingDialogCall(shareDialog.present());

        } else {
            // Fallback. For example, publish the post using the Feed Dialog
            publishFeedDialog();
        }
    }

    private void publishFeedDialog() {
//        Bundle params = new Bundle();
//        params.putString("name", "Facebook SDK for Android");
//        params.putString("caption", "Build great social apps and get more installs.");
//        params.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
//        params.putString("link", "https://developers.facebook.com/android");
//        params.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

        //test params from maggi
        Bundle params = new Bundle();
        params.putString("name", "This is my photo!");
        params.putString("caption", "Cool pics!");
        params.putString("description", "More test and test");
        //params.putString("link", "https://developers.facebook.com/android");
        params.putString("picture", "http://i.imgur.com/4VPm4.png");

        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(MainActivity.this,
                        Session.getActiveSession(),
                        params)).build();
        feedDialog.show();
    }

    private void publishPicture(){
        Log.d(TAG, "trying to use the share dialog");
        FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
                .setPicture("http://i.imgur.com/4VPm4.png")
                .build();
        uiHelper.trackPendingDialogCall(shareDialog.present());
    }

    //end of mine

    public boolean checkPermissions() {
        Session s = Session.getActiveSession();
        if (s != null) {
            Log.d(TAG, "in check permissions before checking for publish actions");
            Log.d(TAG, "the permissions in check permissions: "+ s.getPermissions());
            return s.getPermissions().contains("publish_actions");
        } else {
            Log.d(TAG, "in check permissions return false");
            return false;
        }
    }

    public void requestPermissions() {
        Session s = Session.getActiveSession();
        if (s != null)
            s.requestNewPublishPermissions(new Session.NewPermissionsRequest(
                    this, PERMISSIONS));

        Log.d(TAG, "trying to ask for publish_actions");
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        //buttonsEnabled(Session.getActiveSession().isOpened());
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        uiHelper.onSaveInstanceState(savedState);
    }

}
